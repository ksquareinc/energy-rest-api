'use strict';

angular.module('energyRestApp.auth', ['energyRestApp.constants', 'energyRestApp.util', 'ngCookies',
    'ui.router'
  ])
  .config(function($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
  });
